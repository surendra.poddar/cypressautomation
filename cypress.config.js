const { defineConfig } = require("cypress");
const fs = require('fs')
module.exports = defineConfig({
  projectId: 'ghkttb',
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
  video: true,
  overwrite:true,
  screenshotsFolder:"cypress/screenshot/surendra",
  viewportHeight: 1060,
viewportWidth: 1400,
chromeWebSecurity: false,
  reporter: "mochawesome",
   "reporterOptions": {
      "reportDir": "cypress/results",
      "overwrite": false,
      "html": false,
      "json": true,"mochaFile": "cypress/results/results-[hash].xml",
      "toConsole": true
   }
});
