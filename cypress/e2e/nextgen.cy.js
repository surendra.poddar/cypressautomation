describe("Cypress Exception Handling", () => {
  
  it("Test Failure when trying to find incorrect locator- error Message", () =>
  {
   cy.on('uncaught:exception', (err, runnable) => {
     return false;
   }); 
   cy.intercept('/en-US').as('nextgenStore')
     const partnerUrl = "http://goldenpantry.ourproshop.com/"
     cy.visit("http://goldenpantry.ourproshop.com/en-US", {timeout:8000})
//const partnerUrl = "https://nextgen.ourproshop.com/en-US"
   //cy.visit("https://nextgen.ourproshop.com/en-US", {timeout:8000})
 //  cy.visit("https://abc-ltd-store-1.dev.proshopnext.com/en-US")
   cy.wait('@nextgenStore')
   
   //Sign in page
   cy.get('[id="sign-in-email"]').type("admin.user@dev.com")
   cy.get('[id="signInPassword"]').type("Admin@123")
   cy.screenshot('actions/goldenpantry/login')
   cy.contains('button','Sign in').click()
   
   cy.wait(9000)
   cy.screenshot('actions/goldenpantry/successlogin')
 
  //Click on See All
  //cy.get('a[href *="/en-US/branded-product-catalog"]').contains('See all').click({ force: true })
  cy.get('.swan-display-flex.swan-mb-5 > a').each(($el) => {
      const herf = $el.attr('href');
      var valu = herf.split('/')
      cy.log("HREF : ",valu[valu.length-1]);
  
      //cy.log("HREF : ",getProductInfo(herf));
      // then I will do your test:
      cy.visit(partnerUrl+herf),{timeout:5000};
      //cy.title().should('include', 'Text');
  
  });

  cy.screenshot('actions/goldenpantry/SeeAll')
  
  //wait for page load
  //cy.wait(9000)
 
  cy.visit('https://goldenpantry.ourproshop.com/en-US/branded-products/10c8fad9-2a0b-a256-5ad5-b4cece470f2a')
   //cy.get('.swan-grid-container >:nth-child(3) > :nth-child(5) > a [href*="/en-US/branded-product-catalog/*"]').click({ force: true })
   cy.screenshot('actions/goldenpantry/10c8fad9-2a0b-a256-5ad5-b4cece470f2a')
/*   
   cy.get('[value="Front"]').click()
   cy.screenshot('actions/nextgen_1/ZenScooter_front')
   cy.get('[value="Back"]').click()
   cy.screenshot('actions/nextgen_1/ZenScooter_back')
*/  
   cy.contains('button','Start designing').click()
   //cy.get('[value="Start designing"]').click()
 
   cy.wait(10000)
   cy.screenshot('actions/goldenpantry/Designer')
  
   
   cy.get('[data-testid="previewButton"]').click({timeout:5000})
   cy.screenshot('actions/goldenpantry/PreviewScreen')


   cy.get('[data-testid="studioPreview__FrontButton"]').click()
   cy.screenshot('actions/goldenpantry/FrontPreviewScreen')
   cy.get('[data-testid="studioPreview__BackButton"]').click()
   cy.screenshot('actions/goldenpantry/BackPreviewScreen')

   cy.get('.swan-modal-dialog-close-button').click()

cy.contains('button','Next').should('be.visible').click({timeout:5000})
cy.screenshot('actions/goldenpantry/NextButton')

cy.contains('button','Continue').should('be.visible').click({timeout:5000})
cy.screenshot('actions/goldenpantry/ContinueButton')

cy.get('[type="checkbox"]').should('be.visible').check()
cy.screenshot('actions/goldenpantry/AddToCartScreen')
cy.contains('button','Add to cart').click({timeout:8000})
cy.screenshot('actions/goldenpantry/CheckoutScreen')

/*
cy.get('a.swan-button.swan-button-skin-primary.swan-button-full-width').should('be.visible')
cy.contains('button','Checkout').should('be.visible')
cy.contains('button','Continue Shopping').should('be.visible')
*/

 }) 
 
 });